syntax on
" set number relativenumber
colorscheme dogrun
set t_Co=256
set noshowmode 
set tabstop=2
set shiftwidth=0

let g:lightline = {
      \ 'colorscheme': 'seoul256',
      \ }

" Plugins will be downloaded under the specified directory.
call plug#begin('~/.vim/plugged')

" Declare the list of plugins.
" Shorthand notation; fetches https://github.com/junegunn/vim-easy-align
Plug 'junegunn/vim-easy-align'
" Any valid git URL is allowed
Plug 'https://github.com/junegunn/vim-github-dashboard.git'

Plug 'tpope/vim-sensible'
Plug 'junegunn/seoul256.vim'

"NerdTree
Plug 'scrooloose/nerdtree'
map <C-o> :NERDTreeToggle<CR>
map <S-Right> :tabn<CR>
map <S-Left>  :tabp<CR>

Plug 'nathanaelkane/vim-indent-guides'
Plug 'ctrlpvim/ctrlp.vim'
Plug 'tpope/vim-surround'
Plug 'mattn/emmet-vim'
Plug 'terryma/vim-multiple-cursors'

Plug 'myusuf3/numbers.vim'
nnoremap <F3> :NumbersToggle<CR>
nnoremap <F4> :NumbersOnOff<CR>

Plug 'itchyny/lightline.vim'
Plug 'istib/vifm.vim'
map <leader>ed :EditVifm<CR>
map <leader>vs :VsplitVifm<CR>

Plug 'ap/vim-css-color', { 'for': [ 'lua' ] }

" List ends here. Plugins become visible to Vim after this call.
call plug#end()
