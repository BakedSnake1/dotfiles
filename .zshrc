export BROWSER='brave'
export EDITOR='vim'
export VISUAL='vim'
export TERM='xterm-256color'
#export LIBGL_ALWAYS_SOFTWARE=1
#export MESA_GL_VERSION_OVERRIDE=4.6
#export MESA_GLSL_VERSION_OVERRIDE=460

### SET BAT AS MANPAGER ###
export MANPAGER="sh -c 'col -bx | bat -l man -p'"
### SET VIM AS MANPAGER ###
#export MANPAGER="/bin/sh -c \"col -b | vim --not-a-term -c 'set ft=man ts=8 nomod nolist noma' -\""

PROMPT='%(?.%F{green} .%F{red} )%f %B%F{6}%~%b%F{13}[%B%F{11}%#%F{13}%b]%f '

# SEARCH HISTORY SUBSTRING
source /usr/share/zsh/plugins/zsh-history-substring-search/zsh-history-substring-search.zsh
export HISTFILE=~/.zsh_history
export HISTSIZE=100000      # Nearly infinite history; essential to building a cli 'library' to use with fzf/etc
export SAVEHIST=100000
setopt share_history        # share it across sessions
setopt extended_history     # add timestamps to history
setopt hist_ignore_all_dups # don't record dupes in history
setopt hist_ignore_space    # remove command line from history list when first character on the line is a space
setopt hist_reduce_blanks   # remove superflous blanks

# AUTO-COMPLETION
source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh
setopt notify
setopt correct
setopt auto_cd
setopt auto_list

# SYNTAX HIGHLIGHTING
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
ZSH_HIGHLIGHT_STYLES[suffix-alias]=fg=cyan,underline
ZSH_HIGHLIGHT_STYLES[precommand]=fg=cyan,underline
ZSH_HIGHLIGHT_STYLES[arg0]=fg=cyan
ZSH_HIGHLIGHT_HIGHLIGHTERS=(main brackets pattern cursor)
ZSH_HIGHLIGHT_PATTERNS=('rm -rf *' 'fg=white,bold,bg=red')

# GIT
autoload -Uz vcs_info
precmd_vcs_info() { vcs_info }
precmd_functions+=( precmd_vcs_info )
setopt prompt_subst
RPROMPT=\$vcs_info_msg_0_
zstyle ':vcs_info:*' check-for-changes true
zstyle ':vcs_info:*' unstagedstr ' * '
zstyle ':vcs_info:*' stagedstr ' + '
zstyle ':vcs_info:git:*' formats '%F{13}(%F{11}%b%F{1}%u%c%F{13})%F{6}%r%f'
zstyle ':vcs_info:git:*' actionformats '%F{13}(%F{11}%b%F{1}%a%u%c%F{13})%F{6}%r%f'
zstyle ':vcs_info:*' enable git

### Aliases ###

# navigation
alias ..='cd ..'
alias ls='ls --color=auto'
alias la='ls -lah'
# adding flags
alias cp="cp -i"                          # confirm before overwriting something
alias rm='rm -i'                          # >>      >>     >>          >>
alias RM='rm -rf'
# the terminal rickroll
alias rr='curl -s -L https://raw.githubusercontent.com/keroserene/rickrollrc/master/roll.sh | bash'
alias wp='cd /mnt/Media/WEBDEV/WhatShouldIListenTo'
alias rc='cd /mnt/Files/PORTFOLIO/recipes'
alias ms='cd /mnt/Files/PORTFOLIO/s-mugiwara-store'
alias fls='cd /mnt/Files'
alias med='cd/mnt/Media'

# Configs
alias grep='grep --color=auto'
alias mpd='mpd .mpd/mpd.conf'
#alias vif='~/.config/vifm/scripts/vifmrun .'
#alias srf='tabbed surf -e'

#MongoDB
alias mongodon='doas systemctl start mongodb.service'
alias mongodoff='doas systemctl stop mongodb.service'
alias mongov='mongo vagrantz.xyz --authenticationDatabase 'admin' -u 'admin' -p'
alias mongov2='mongo inter.kitchen --authenticationDatabase 'test' -u 'admin' -p'

# Android
alias phone='jmtpfs ~/mnt'
alias unphone='fusermount -u ~/mnt'

# Virt-Manager
alias start-virt='doas systemctl start libvirtd && doas systemctl start virtlogd'
alias stop-virt='doas systemctl stop libvirtd && doas systemctl stop virtlogd'

# Date
alias timsync='doas timedatectl set-ntp true'
alias timdsync='doas timedatectl set-ntp false'

# Shutdown
alias dsd='doas shutdown now'

### RANDOM COLOR SCRIPT ###
# colorscript -e ghosts
# colorscript -e rupees
# colorscript -e fade
# colorscript -e dna
# colorscript -e square
#
## PRINT MULTIPLE RAMDOMLY
Result=$((RANDOM%3))
if [[ ${Result} -eq 0 ]]; then
    colorscript -e ghosts
elif [[ ${Result} -eq 1 ]]; then
    colorscript -e fade
elif [[ ${Result} -eq 2 ]]; then
    colorscript -e dna
fi

## GAMES
alias metin2=' WINEPREFIX=$HOME/.local/share/wineprefixes/Rubinum WINEARCH=win32 wine /mnt/Media/Rubinum/RubinumLauncher.exe'

# StarShip
#eval "$(starship init zsh)"

