### EXPORT ###
set fish_greeting                      # Supresses fish's intro message
set EDITOR "vim"
set PATH $PATH /home/cg/Documents/bashScripts
export PATH
### AUTOCOMPLETE AND HIGHLIGHT COLORS ###
set fish_color_normal brcyan
set fish_color_autosuggestion '#7d7d7d'
set fish_color_command brcyan
set fish_color_error '#ff6c6b'
set fish_color_param brcyan

### VI SHORTCUTZ 
function fish_user_key_bindings
    # fish_default_key_bindings
    fish_vi_key_bindings
end

export MANPAGER="/bin/sh -c \"col -b | vim --not-a-term -c 'set ft=man ts=8 nomod nolist noma' -\""

#---------------------------------------------
# Aliases
#---------------------------------------------

# Terminal use
#alias ..='cd ..'
alias ls='ls --color=auto'
#alias la='ls -A'
#alias lh='ls -lah'
alias mv='mv -i'
alias MV='mv -rf'
alias rm='rm -i'
alias RM='rm -rf'
alias wp='cd /mnt/Media/WEBDEV/WhatShouldIListenTo'

#Windows
alias mfile='sudo mount /dev/sda1 /mnt/Files'
alias umfile='sudo umount /mnt/Files'
alias mmedia='sudo mount /dev/sda2 /mnt/Media'
alias umedia='sudo umount /mnt/Media'
alias mmisc='sudo mount /dev/sda3 /mnt/Misc'
alias umisc='sudo umount /mnt/Misc'

# Configs
alias grep='grep --color=auto'
alias mpd='mpd .mpd/mpd.conf'
#alias vif='~/.config/vifm/scripts/vifmrun .'
#alias srf='tabbed surf -e'

#MongoDB
alias mongodon='sudo systemctl start mongodb.service'
alias mongodoff='sudo systemctl stop mongodb.service'
alias mongov="mongo vagrantz.xyz --authenticationDatabase 'admin' -u 'admin' -p"

# Android
alias phone='jmtpfs ~/mnt'
alias unphone='fusermount -u ~/mnt'

# Virt-Manager
alias start-virt='sudo systemctl start libvirtd && sudo systemctl start virtlogd'
alias stop-virt='sudo systemctl stop libvirtd && sudo systemctl stop virtlogd'

# Shutdown
alias shutdown='sudo shutdown now'

# Rick Ashley
alias rr='curl -s -L https://raw.githubusercontent.com/keroserene/rickrollrc/master/roll.sh |
bash'

# Greet
bs-greet

# Starship
starship init fish | source
