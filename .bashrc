#
# ~/.bashrc
#

### RANDOM COLOR SCRIPT ###
colorscript -e ghosts

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
PS1='\w:$(git branch 2>/dev/null | grep '^*' | colrm 1 2)\e[41;0;32m[$] '

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=1000
HISTFILESIZE=2000

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
    xterm-color|$TERM-256color) color_prompt=yes;;
esac

#Powerline-shell
#function _update_ps1() {
#    PS1=$(powerline-shell $?)
#}

#if [[ $TERM != linux && ! $PROMPT_COMMAND =~ _update_ps1 ]]; then
#    PROMPT_COMMAND="_update_ps1; $PROMPT_COMMAND"
#fi

# rtorrent color
#for i in $(rtxmlrpc system.listMethods | egrep '^ui.color.[^.]+$'); do
#    echo $i = $(rtxmlrpc -r $i | tr "'" '"') ;
#done

# Aliases
alias ..='cd ..'
alias mwin='sudo mount /dev/sda2 /mnt/windows'
alias win='cd /mnt/windows/Users/Gebruiker'
alias ls='ls --color=auto'
alias grep='grep --color=auto'
alias mv='mv -i'
alias MV='mv -rf'
alias rm='rm -i'
alias RM='rm -rf'
alias rr='curl -s -L https://raw.githubusercontent.com/keroserene/rickrollrc/master/roll.sh | bash'
alias vif='~/.config/vifm/scripts/vifmrun .'
alias mp='mpd ~/.mpd/mpd.conf'
alias srf='tabbed surf -e'
alias shutdown='sudo shutdown now'
alias mpd='mpd .mpd/mpd.conf'

# Trackpad
xinput set-prop 10 300 1
xinput set-prop 10 320 1
